﻿namespace Dungeon_Lurker
{
    public enum Menu
    {
        None,
        StartGame,
        CreateHero,
        CreateMonster,
        CreateWeapon,
        CreateArmor,
        CreateAmulet,
        HighScores,
        QuitGame
    }
}