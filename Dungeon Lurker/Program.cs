﻿using BusinessLogic;
using System;
using Ninject;

namespace Dungeon_Lurker
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new DataAccessModule(), new BusinessLogicModule());

            var adventure = kernel.Get<Adventure>();
            var gameCreation = kernel.Get<GameCreation>();

            bool doContinue = true;

            while (doContinue)
            {
                Console.Clear();

                Console.WriteLine(DisplayControl.Title);
                Console.WriteLine($"\n1.Start Game" +
                                  $"\n2.Create Hero" +
                                  $"\n3.Create Monster" +
                                  $"\n4.Create Weapon" +
                                  $"\n5.Create Armor" +
                                  $"\n6.Create Amulet" +
                                  $"\n7.High Scores" +
                                  $"\n8.Quit Game");

                Menu menu = (Menu)ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 8);

                switch (menu)
                {
                    case Menu.StartGame:
                        StartGame(adventure, gameCreation);
                        break;

                    case Menu.CreateHero:
                        CreateHero(adventure, gameCreation);
                        break;

                    case Menu.CreateMonster:
                        CreateMonster(gameCreation);
                        break;

                    case Menu.CreateWeapon:
                        CreateWeapon(gameCreation);
                        break;

                    case Menu.CreateArmor:
                        CreateArmor(gameCreation);
                        break;

                    case Menu.CreateAmulet:
                        CreateAmulet(gameCreation);
                        break;

                    case Menu.HighScores:
                        gameCreation.DisplayHighScores();
                        Console.ReadLine();
                        break;

                    case Menu.QuitGame:
                        doContinue = false;
                        break;
                }
            }
        }

        private static void StartGame(Adventure adventure, GameCreation gameCreation)
        {
            if (gameCreation.CheckIfElementsInDatabase())
            {
                Console.WriteLine("\nAdd missing elements" +
                                  "\nPress any key to continue");
                Console.ReadLine();
                return;
            }

            gameCreation.DisplayHeroes();
            Console.WriteLine("\nChoose your hero (by name)");
            string heroChoice = ExceptionPrevention.StringLimit(Console.ReadLine());
            adventure.ChooseHero(heroChoice);
        }

        private static void CreateHero(Adventure adventure, GameCreation gameCreation)
        {
            bool doContinue = true;

            while (doContinue)
            {
                Console.Clear();
                gameCreation.DisplayHeroes();

                Console.WriteLine("\n1.Create Hero" +
                                  "\n2.Delete Hero" +
                                  "\n3.Exit");
                int choice = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 3);

                switch (choice)
                {
                    case 1:
                        HeroCreation(adventure, gameCreation);
                        break;

                    case 2:
                        Console.WriteLine("Choose your hero for permanent removal (by name)");
                        string deleteHero = Console.ReadLine();
                        gameCreation.DeleteHero(deleteHero);
                        break;

                    case 3:
                        doContinue = false;
                        break;
                }
            }
        }

        private static void CreateMonster(GameCreation gameCreation)
        {
            bool doContinue = true;

            while (doContinue)
            {
                Console.Clear();
                gameCreation.DisplayMonsters();

                Console.WriteLine("\n1.Create Monster" +
                                  "\n2.Delete Monster" +
                                  "\n3.Exit");
                int choice = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 3);

                switch (choice)
                {
                    case 1:
                        MonsterCreation(gameCreation);
                        break;

                    case 2:
                        Console.WriteLine("Choose your Monster for permanent removal (by name)");
                        string deleteMonster = Console.ReadLine();
                        gameCreation.DeleteMonster(deleteMonster);
                        break;

                    case 3:
                        doContinue = false;
                        break;
                }
            }
        }

        private static void CreateWeapon(GameCreation gameCreation)
        {
            bool doContinue = true;

            while (doContinue)
            {
                Console.Clear();
                gameCreation.DisplayWeapons();

                Console.WriteLine("\n1.Create Weapon" +
                                  "\n2.Delete Weapon" +
                                  "\n3.Exit");
                int choice = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 3);

                switch (choice)
                {
                    case 1:
                        WeaponCreation(gameCreation);
                        break;

                    case 2:
                        Console.WriteLine("Choose your Weapon for permanent removal (by name)");
                        string deleteWeapon = Console.ReadLine();
                        gameCreation.DeleteWeapon(deleteWeapon);
                        break;

                    case 3:
                        doContinue = false;
                        break;
                }
            }
        }

        private static void CreateArmor(GameCreation gameCreation)
        {
            bool doContinue = true;

            while (doContinue)
            {
                Console.Clear();
                gameCreation.DisplayArmors();

                Console.WriteLine("\n1.Create Armor" +
                                  "\n2.Delete Armor" +
                                  "\n3.Exit");
                int choice = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 3);

                switch (choice)
                {
                    case 1:
                        ArmorCreation(gameCreation);
                        break;

                    case 2:
                        Console.WriteLine("Choose your Armor for permanent removal (by name)");
                        string deleteArmor = Console.ReadLine();
                        gameCreation.DeleteArmor(deleteArmor);
                        break;

                    case 3:
                        doContinue = false;
                        break;
                }
            }
        }

        private static void CreateAmulet(GameCreation gameCreation)
        {
            bool doContinue = true;

            while (doContinue)
            {
                Console.Clear();
                gameCreation.DisplayAmulets();

                Console.WriteLine("\n1.Create Amulet" +
                                  "\n2.Delete Amulet" +
                                  "\n3.Exit");
                int choice = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 3);

                switch (choice)
                {
                    case 1:
                        AmuletCreation(gameCreation);
                        break;

                    case 2:
                        Console.WriteLine("Choose your Amulet for permanent removal (by name)");
                        string deleteAmulet = Console.ReadLine();
                        gameCreation.DeleteAmulet(deleteAmulet);
                        break;

                    case 3:
                        doContinue = false;
                        break;
                }
            }
        }


        private static void HeroCreation(Adventure adventure, GameCreation gameCreation)
        {
            Console.Write("Enter your hero's name: ");
            string name = ExceptionPrevention.StringLimit(Console.ReadLine());
            gameCreation.CreateNewHero(name);
        }

        private static void MonsterCreation(GameCreation gameCreation)
        {
            Console.Write("Enter your monster's name: ");
            string name = ExceptionPrevention.StringLimit(Console.ReadLine());
            Console.WriteLine(name);
            Console.WriteLine("Set your monster's attributes: ");
            Console.WriteLine("Set Strength to (1-30):");
            int strength = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 30);
            Console.WriteLine("Set Dexterity to (1-30):");
            int dexterity = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 30);
            Console.WriteLine("Set Vitality to (1-30):");
            int vitality = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 30);
            Console.WriteLine("Set Physical Defense to (1-30):");
            int strengthDefense = ExceptionPrevention.CheckVariable(Console.ReadLine(), 0, 100);
            Console.WriteLine($"Set how many experience you gain after {name}'s death (0-1000):");
            int expWorth = ExceptionPrevention.CheckVariable(Console.ReadLine(), 0, 1000);
            Console.WriteLine("Set Level to (1-20):");
            int level = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 20);
            Console.WriteLine($"Set how many points you gain after {name}'s death (0-1000):");
            int pointsWorth = ExceptionPrevention.CheckVariable(Console.ReadLine(), 0, 1000);

            gameCreation.CreateNewMonster(name, strength, dexterity, vitality, strengthDefense,
                expWorth, level, pointsWorth);
        }

        private static void WeaponCreation(GameCreation gameCreation)
        {
            Console.Write("Enter your weapons's name: ");
            string name = ExceptionPrevention.StringLimit(Console.ReadLine());
            Console.WriteLine("Set your weapon's strength (1-100): ");
            int strengthModifier = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 100);
            Console.WriteLine("Set level to (1-20):");
            int level = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 20);

            gameCreation.CreateNewWeapon(name, strengthModifier, level);
        }

        private static void ArmorCreation(GameCreation gameCreation)
        {
            Console.Write("Enter your armor's name: ");
            string name = ExceptionPrevention.StringLimit(Console.ReadLine());
            Console.WriteLine("Set your armors's defense (1-100): ");
            int defenseModifier = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 100);
            Console.WriteLine("Set level to (1-20):");
            int level = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 20);

            gameCreation.CreateNewArmor(name, defenseModifier, level);
        }

        private static void AmuletCreation(GameCreation gameCreation)
        {
            Console.Write("Enter your amulet's name: ");
            string name = ExceptionPrevention.StringLimit(Console.ReadLine());
            Console.WriteLine("Set your amulet's attribute type:" +
                              "\n1.Strength" +
                              "\n2.Magic" +
                              "\n3.Dexterity" +
                              "\n4.Vitality ");
            int choice = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 4);
            string attributeType = String.Empty;

            switch (choice)
            {
                case 1:
                    attributeType = "Strength";
                    break;

                case 2:
                    attributeType = "Magic";
                    break;

                case 3:
                    attributeType = "Dexterity";
                    break;

                case 4:
                    attributeType = "Vitality";
                    break;
            }

            Console.WriteLine("Set your amulet's attribute value (-20-20): ");
            int attributeModifier = ExceptionPrevention.CheckVariable(Console.ReadLine(), -20, 20);
            Console.WriteLine("Set level to (1-20):");
            int level = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 20);

            gameCreation.CreateNewAmulet(name, attributeType, attributeModifier, level);
        }
    }
}
