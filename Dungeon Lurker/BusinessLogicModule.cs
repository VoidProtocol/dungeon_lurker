﻿using BusinessLogic;
using Ninject.Modules;

namespace Dungeon_Lurker
{
    class BusinessLogicModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAdventure>().To<Adventure>();
            Bind<IGameCreation>().To<GameCreation>();
            Bind<ILevelManager>().To<LevelManager>();
            Bind<IPlayerFight>().To<PlayerFight>();
        }
    }
}
