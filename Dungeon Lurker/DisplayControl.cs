﻿using System;

namespace Dungeon_Lurker
{
    class DisplayControl
    {
        public static string Title = @"
  ____                                       _               _             
 |  _ \ _   _ _ __   __ _  ___  ___  _ __   | |   _   _ _ __| | _____ _ __ 
 | | | | | | | '_ \ / _` |/ _ \/ _ \| '_ \  | |  | | | | '__| |/ / _ \ '__|
 | |_| | |_| | | | | (_| |  __/ (_) | | | | | |__| |_| | |  |   <  __/ |   
 |____/ \__,_|_| |_|\__, |\___|\___/|_| |_| |_____\__,_|_|  |_|\_\___|_|   
                    |___/                                                  ";

        public static void SetWindowSize(int windowWidth, int windowHeight)
        {
            if (windowWidth > Console.LargestWindowWidth)
            {
                windowWidth = Console.LargestWindowWidth;
            }

            if (windowHeight > Console.LargestWindowWidth)
            {
                windowHeight = Console.LargestWindowWidth;
            }

            Console.SetWindowSize(windowWidth, windowHeight);
        }

        public static void WindowCenter()
        {
            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
        }

        public static void WindowCenter2(string text)
        {
            Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", text));
        }
    }
}
