﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class Rental
    {
        public int Id { get; set; }
        [Column(TypeName="datetime2")]
        public DateTime StartDate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime ReturnDate { get; set; }
        public double TotalPrice { get; set; }
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }
        public int CarId { get; set; }
        public virtual Car Car { get; set; }
        public int? DiscountId { get; set; }
        public virtual Discount Discount { get; set; }
    }
}
