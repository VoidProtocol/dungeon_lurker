﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class CarRentalDbContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Rental> Rentals { get; set; }
        public DbSet<Discount> Discounts { get; set; }

        public CarRentalDbContext() : base("CarRentalDbConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CarRentalDbContext, Migrations.Configuration>());
        }
    }
}
