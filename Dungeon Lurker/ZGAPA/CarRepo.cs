﻿using DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Repos
{
    public class CarRepo
    {
        private CarRentalDbContext _dbContext;

        public CarRepo(CarRentalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddCar(Car car)
        {
            _dbContext.Cars.Add(car);
            _dbContext.SaveChanges();
        }

        public IList<Car> GetAllCars()
        {
            return _dbContext.Cars.ToList();
        }

        public Car GetById(int id)
        {
            return _dbContext.Cars.Find(id);
        }

        public void Update(Car car)
        {
            var dbCar = _dbContext.Cars.Find(car.Id);
            dbCar = car;
            _dbContext.SaveChanges();
        }
    }
}
