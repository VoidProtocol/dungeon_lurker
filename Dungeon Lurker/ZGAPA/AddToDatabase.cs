﻿using DataAccess;
using DataAccess.Entities;
using DataAccess.Repos;
using System.Linq;


namespace BusinessLogic
{
    public class AddToDatabase 
    {
        private CarRentalDbContext _dbContext;
        private ModelRepo _modelRepo;
        private CarRepo _carRepo;
        private ClientRepo _clientRepo;
        private DiscountRepo _discountRepo;
        private RentalRepo _rentalRepo;

        public AddToDatabase()
        {
            _dbContext = new CarRentalDbContext();
            _modelRepo = new ModelRepo(_dbContext);
            _carRepo = new CarRepo(_dbContext);
            _clientRepo = new ClientRepo(_dbContext);
            _discountRepo = new DiscountRepo(_dbContext);
            _rentalRepo = new RentalRepo(_dbContext);
        }

        public Client AddClient (long pesel, string name, string surname)
        {
            var clientExists = _clientRepo.GetAllClients().SingleOrDefault(c => c.Pesel == pesel) != null;
            if (clientExists) return null;
            var newClient = new Client()
            {
                Name = name,
                Surname = surname,
                Pesel = pesel
            };
            _clientRepo.AddClient(newClient);
            return newClient;
        }

        public Model AddModel(string brandName, string modelName, double price)
        {
            var modelExists = _modelRepo.GetAllModels().SingleOrDefault(m => m.CarBrand == brandName && m.CarModel == modelName) != null;
            if (modelExists) return null;
            var newModel = new Model(brandName, modelName, price);
            _modelRepo.Add(newModel);
            return newModel;
        }

        public Car AddCar(string registrationNumber, string modelName)
        { 
            var model = _modelRepo.GetAllModels().FirstOrDefault(m => m.CarModel == modelName);
            var carExists = _carRepo.GetAllCars().SingleOrDefault(c => c.RegistrationNumber == registrationNumber) != null;
            if (model == null || carExists) return null;           
            var newCar = new Car()
                {
                    ModelId = model.Id,
                    RegistrationNumber = registrationNumber,
                    IsRented = false
                };
             _carRepo.AddCar(newCar);
             return newCar;        
        }

        public Discount AddDiscount(string discountName, int discountAmount)
        {
            var discountExists = _discountRepo.GetAllDiscounts().SingleOrDefault(d => d.DiscountName == discountName) != null;
            if (discountExists) return null;
            var newDiscount = new Discount()
            {
               DiscountAmount = discountAmount,
               DiscountName = discountName
            };
            _discountRepo.AddDiscount(newDiscount);
            return newDiscount;
        }
    }
}