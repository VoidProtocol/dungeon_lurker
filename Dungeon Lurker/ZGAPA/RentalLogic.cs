﻿using System;
using DataAccess;
using DataAccess.Entities;
using DataAccess.Repos;
using System.Linq;


namespace BusinessLogic
{
    public class RentalLogic
    {
        private CarRentalDbContext _dbContext;
        private ModelRepo _modelRepo;
        private CarRepo _carRepo;
        private ClientRepo _clientRepo;
        private DiscountRepo _discountRepo;
        private RentalRepo _rentalRepo;

        public RentalLogic()
        {
            _dbContext = new CarRentalDbContext();
            _modelRepo = new ModelRepo(_dbContext);
            _carRepo = new CarRepo(_dbContext);
            _clientRepo = new ClientRepo(_dbContext);
            _discountRepo = new DiscountRepo(_dbContext);
            _rentalRepo = new RentalRepo(_dbContext);
    }

        public string DisplayDiscountList()
        {
            var discountList = "0. No discount\n";
            var discounts = _discountRepo.GetAllDiscounts();
            for (int i = 0; i < discounts.Count; i++)
            {
                discountList = discountList + $"{i + 1}. {discounts[i].DiscountName} - {discounts[i].DiscountAmount}% \n";
            }
            return discountList;
        }

        public string DisplayAvailableModels()
        {
            var availableModelsList = string.Empty;
            var availableModels = _carRepo.GetAllCars().Where(r => r.IsRented == false).GroupBy(c => c.Model).Select(x => x.First()).ToList();
            for (int i = 0; i < availableModels.Count; i++)
            {
                availableModelsList = availableModelsList +
                    $"{i + 1}. {availableModels[i].Model.CarBrand} {availableModels[i].Model.CarModel} Cena PLN/doba - {availableModels[i].Model.Price}.\n";
            }
            return availableModelsList;
                  
        }

        public string DisplayRentalHistory(long pesel)
        {
            var client = _clientRepo.GetAllClients().FirstOrDefault(m => m.Pesel == pesel);
            if (client == null) return null;
            var rentalHistory = string.Empty;
            var clientRentals = _rentalRepo.GetAllRentals().Where(r => r.ClientId == client.Id);
            foreach (var rental in clientRentals)
            {
                rentalHistory = rentalHistory + $@"Start Date:  {rental.StartDate.ToShortDateString()}
                                                   Return Date: {rental.ReturnDate.ToShortDateString()}
                                                   Car Model: {rental.Car.Model.CarModel}
                                                   Total price: {rental.TotalPrice}     ";
            }
            return rentalHistory;
        }

        public bool NoFreeCar()
        {
            return _carRepo.GetAllCars().Select(c => c.IsRented == false).ToList().Count != 0;
        }

        public Rental StartRental(DateTime startDate, long pesel, int carChoice, int discount)
        {      
            var client = _clientRepo.GetAllClients().FirstOrDefault(m => m.Pesel == pesel);
            if (client == null) return null;
            var availableModels = _carRepo.GetAllCars().Where(r => r.IsRented == false).GroupBy(c => c.Model).Select(x => x.First()).ToList();
            client.TimesHasRented++;
            availableModels[carChoice - 1].IsRented = true;
            var newRental = new Rental()
            {
                StartDate = startDate,
                ClientId = client.Id,
                CarId = availableModels[carChoice - 1].Id,
                DiscountId = null
            };
            if (discount != 0)
            {
                var discounts = _discountRepo.GetAllDiscounts();
                newRental.DiscountId = discounts[discount - 1].Id;
            }
            _rentalRepo.AddRental(newRental);
            return newRental;
        }

        public double? FinishRental(string regNum, DateTime dateReturned)
        {
            var car = _carRepo.GetAllCars().SingleOrDefault(c => c.RegistrationNumber == regNum);
            var rental = car.Rentals.Last(); 

            if (car != null && car.IsRented==true)
            {        
                rental.ReturnDate = dateReturned;
                _rentalRepo.Update(rental);
                car.IsRented = false;                
                _carRepo.Update(car);
                var days = (rental.ReturnDate - rental.StartDate).Days + 1;
                var totalPrice = rental.Client.TimesHasRented >= 10 ? days * car.Model.Price * 0.95 : days * car.Model.Price;
                if (rental.Discount != null)
                {
                    totalPrice = totalPrice * rental.Discount.DiscountAmount/100;
                }
                rental.TotalPrice = totalPrice;
                return totalPrice;
            }
            return null;
        }

        
    }
}