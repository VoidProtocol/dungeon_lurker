﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class Car
    {
        public int Id { get; set; }
        [Column(TypeName=("varchar"))]
        [Index(IsUnique = true)]
        public string RegistrationNumber { get; set; }
        public bool IsRented { get; set; }
        public int ModelId { get; set; }

        public virtual Model Model { get; set; }
        public virtual IList<Rental> Rentals { get; set; }
    }
}