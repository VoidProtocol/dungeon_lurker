using DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;


namespace DataAccess.Repos
{
    public class RentalRepo
    {
        private CarRentalDbContext _dbContext;

        public RentalRepo(CarRentalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddRental(Rental rental)
        {
            _dbContext.Rentals.Add(rental);
            _dbContext.SaveChanges();
        }

        public IList<Rental> GetAllRentals()
        {
            return _dbContext.Rentals.ToList();
        }

        public Rental GetById(int id)
        {
            return _dbContext.Rentals.Find(id);
        }

        public void Update(Rental rental)
        {
            var dbRental = _dbContext.Rentals.Find(rental.Id);
            dbRental = rental;
            _dbContext.SaveChanges();
        }
    }
}
