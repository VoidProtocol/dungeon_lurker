﻿using System;
using System.Collections.Generic;
using DataAccess.Entities;
using DataAccess.Repos;

namespace BusinessLogic
{
    public class GameCreation : IGameCreation
    {
        private readonly IHeroRepo _heroRepo;
        private readonly IMonsterRepo _monsterRepo;
        private readonly IHighScoreRepo _highScoreRepo;
        private readonly IWeaponRepo _weaponRepo;
        private readonly IArmorRepo _armorRepo;
        private readonly IAmuletRepo _amuletRepo;
        private readonly ILevelManager _levelManager;

        public GameCreation(IHeroRepo heroRepo, IMonsterRepo monsterRepo, IHighScoreRepo highScoreRepo, 
            IWeaponRepo weaponRepo, IArmorRepo armorRepo, IAmuletRepo amuletRepo, ILevelManager levelManager)
        {
            _heroRepo = heroRepo;
            _monsterRepo = monsterRepo;
            _highScoreRepo = highScoreRepo;
            _weaponRepo = weaponRepo;
            _armorRepo = armorRepo;
            _amuletRepo = amuletRepo;
            _levelManager = levelManager;
        }

        public void CreateNewHero(string name)
        {
            Hero hero = new Hero()
            {
                Name = name,
                Strength = 1,
                Magic = 1,
                Dexterity = 1,
                Vitality = 1,
                FullHealth = GameRules.AttributesMultiplying(1, GameRules.HeroVitMulti),
                CurrentHealth = GameRules.AttributesMultiplying(1, GameRules.HeroVitMulti),
                StrengthDefense = 0,
                Exp = 0,
                Level = 1,
                Points = 0,
                Alive = true,
                CurrentWeapon = null,
                CurrentArmor = null,
                CurrentAmulet = null
            };

            _levelManager.AttributeRaise(hero, GameRules.AmountOfPointsAfterLvlUp);
            _heroRepo.CreateHero(hero);
        }

        public void CreateNewMonster(string name, int strength, int dexterity, int vitality,
            int strengthDefense, int expWorth, int level, int pointsWorth)
        {
            Monster monster = new Monster()
            {
                Name = name,
                Strength = strength,
                Dexterity = dexterity,
                Vitality = vitality,
                FullHealth = GameRules.AttributesMultiplying(vitality, GameRules.MonsterVitMulti),
                CurrentHealth = GameRules.AttributesMultiplying(vitality, GameRules.MonsterVitMulti),
                StrengthDefense = strengthDefense,
                MagicDefense = 0,
                ExpWorth = expWorth,
                Level = level,
                PointsWorth = pointsWorth
            };

            _monsterRepo.CreateMonster(monster);
        }

        public void CreateNewHighScore(string name, int points, string filePath)
        {
            HighScore highScore = new HighScore()
            {
                Name = name,
                Points = points,
                Date = DateTime.Now.ToString("G"),
                FilePath = filePath
            };

            _highScoreRepo.CreateHighScore(highScore);
        }

        public void CreateNewWeapon(string name, int strengthModifier, int level)
        {
            Weapon weapon = new Weapon()
            {
                Name = name,
                StrengthModifier = strengthModifier,
                Level = level
            };

            _weaponRepo.CreateWeapon(weapon);
        }

        public void CreateNewArmor(string name, int defenseModifier, int level)
        {
            Armor armor = new Armor()
            {
                Name = name,
                DefenseModifier = defenseModifier,
                Level = level
            };

            _armorRepo.CreateArmor(armor);
        }

        public void CreateNewAmulet(string name, string attributeType, int attributeModifier, int level)
        {
            Amulet amulet = new Amulet()
            {
                Name = name,
                AttributeType = attributeType,
                AttributeModifier = attributeModifier,
                Level = level
            };

            _amuletRepo.CreateAmulet(amulet);
        }

        public void DisplayHeroes()
        {
            List<Hero> heroes = _heroRepo.RetrieveHeroes();

            if (heroes.Count == 0)
            {
                Console.WriteLine("No Heroes in database");
                return;
            }

            foreach (Hero hero in heroes)
            {
                Console.WriteLine($"\nName: {hero.Name}" +
                                  $"\nStrength: {hero.Strength}" +
                                  $"\nMagic: {hero.Magic}" +
                                  $"\nDexterity: {hero.Dexterity}" +
                                  $"\nVitality: {hero.Vitality}" +
                                  $"\nHealth: {hero.CurrentHealth}/{hero.FullHealth}" +
                                  $"\nDefense: {hero.StrengthDefense}" +
                                  $"\nExp points: {hero.Exp}" +
                                  $"\nLevel: {hero.Level}" +
                                  $"\nPoints: {hero.Points}" +
                                  $"\nWeapon: {DisplayHeroWeapon(hero)}" +
                                  $"\nArmor: {DisplayHeroArmor(hero)}" +
                                  $"\nAmulet: {DisplayHeroAmulet(hero)}" +
                                  $"\nStatus: {DisplayStatus(hero.Alive)}");
            }
        }

        private string DisplayHeroWeapon(Hero hero)
        {
            if (hero.CurrentWeapon == null)
            {
                return "none";
            }
            else
            {
                return $"{hero.CurrentWeapon.Name} (+{hero.CurrentWeapon.StrengthModifier} strength)";
            }
        }

        private string DisplayHeroArmor(Hero hero)
        {
            if (hero.CurrentArmor == null)
            {
                return "none";
            }
            else
            {
                return $"{hero.CurrentArmor.Name} ({hero.CurrentArmor.DefenseModifier} defense)";
            }
        }

        private string DisplayHeroAmulet(Hero hero)
        {
            if (hero.CurrentAmulet == null)
            {
                return "none";
            }
            else
            {
                return $"{hero.CurrentAmulet.Name} (+{hero.CurrentAmulet.AttributeModifier} " +
                       $"{hero.CurrentAmulet.AttributeType})";
            }
        }

        private string DisplayStatus(bool alive)
        {
            if (alive)
            {
                return "Alive";
            }
            else
            {
                return "Dead";
            }
        }

        public void DisplayMonsters()
        {
            List<Monster> monsters = _monsterRepo.RetrieveMonsters();

            if (monsters.Count == 0)
            {
                Console.WriteLine("No Monsters in database");
                return;
            }

            foreach (Monster monster in monsters)
            {
                Console.WriteLine($"\nName: {monster.Name}" +
                                  $"\nStrength: {monster.Strength}" +
                                  $"\nDexterity: {monster.Dexterity}" +
                                  $"\nVitality: {monster.Vitality}" +
                                  $"\nHealth: {monster.FullHealth}" +
                                  $"\nPhysical Defense: {monster.StrengthDefense}" +
                                  $"\nMagical Defense: {monster.MagicDefense}" +
                                  $"\nExperience after killing: {monster.ExpWorth}" +
                                  $"\nLevel: {monster.Level}" +
                                  $"\nPoints after killing: {monster.PointsWorth}");
            }
        }

        public void DisplayHighScores()
        {
            List<HighScore> highScores = _highScoreRepo.RetrieveHighScores();

            if (highScores.Count == 0)
            {
                Console.WriteLine("No HighScores in database");
                return;
            }

            foreach (HighScore highScore in highScores)
            {
                Console.WriteLine($"\nName: {highScore.Name}" +
                                  $"\nPoints: {highScore.Points}" +
                                  $"\nDate: {highScore.Date}" +
                                  $"\nFile path to game history: {highScore.FilePath}");
            }
        }

        public void DisplayWeapons()
        {
            List<Weapon> weapons = _weaponRepo.RetrieveWeapons();

            if (weapons.Count == 0)
            {
                Console.WriteLine("No Weapons in database");
                return;
            }

            foreach (Weapon weapon in weapons)
            {
                Console.WriteLine($"\nName: {weapon.Name}" +
                                  $"\n StrengthModifier: +{weapon.StrengthModifier} Strength" +
                                  $"\nLevel: {weapon.Level}");
            }
        }

        public void DisplayArmors()
        {
            List<Armor> armors = _armorRepo.RetrieveArmors();

            if (armors.Count == 0)
            {
                Console.WriteLine("No Armors in database");
                return;
            }

            foreach (Armor armor in armors)
            {
                Console.WriteLine($"\nName: {armor.Name}" +
                                  $"\nDefense: {armor.DefenseModifier}" +
                                  $"\nLevel: {armor.Level}");
            }
        }

        //public void Display(IEnumerable<object> d)
        //{
        //    foreach (var o in d)
        //    {
        //        Console.WriteLine(d);
        //    }
        //}

        public void DisplayAmulets()
        {
            List<Amulet> amulets = _amuletRepo.RetrieveAmulets();

            //Display(amulets);
            //if (amulets.Count == 0)
            //{
            //    Console.WriteLine("No Amulets in database");
            //    return;
            //}

            //AddAbstractClassWithID

            foreach (Amulet amulet in amulets)
            {
                Console.WriteLine($"\nName: {amulet.Name}" +
                                  $"\nAttribute Modifier: {amulet.AttributeModifier} {amulet.AttributeType}" +
                                  $"\nLevel: {amulet.Level}");
            }
        }

        public void DeleteHero(string name)
        {
            List<Hero> heroes = _heroRepo.RetrieveHeroes();
            Hero hero = heroes.Find(h => h.Name == name);

            if (hero == null)
            {
                Console.WriteLine("There's no hero with given name" +
                                  "\nPress any key to continue");
                Console.ReadLine();
                return;
            }

            _heroRepo.RemoveHero(hero);
        }

        public void DeleteMonster(string name)
        {
            List<Monster> monsters = _monsterRepo.RetrieveMonsters();
            Monster monster = monsters.Find(m => m.Name == name);

            if (monster == null)
            {
                Console.WriteLine("There's no monster with given name" +
                                  "\nPress any key to continue");
                Console.ReadLine();
                return;
            }

            _monsterRepo.RemoveMonster(monster);
        }

        public void DeleteWeapon(string name)
        {
            List<Weapon> weapons = _weaponRepo.RetrieveWeapons();
            Weapon weapon = weapons.Find(w => w.Name == name);

            if (weapon == null)
            {
                Console.WriteLine("There's no weapon with given name" +
                                  "\nPress any key to continue");
                Console.ReadLine();
                return;
            }

            _weaponRepo.RemoveWeapon(weapon);
        }

        public void DeleteArmor(string name)
        {
            List<Armor> armors = _armorRepo.RetrieveArmors();
            Armor armor = armors.Find(a => a.Name == name);

            if (armor == null)
            {
                Console.WriteLine("There's no armor with given name" +
                                  "\nPress any key to continue");
                Console.ReadLine();
                return;
            }

            _armorRepo.RemoveArmor(armor);
        }

        public void DeleteAmulet(string name)
        {
            List<Amulet> amulets = _amuletRepo.RetrieveAmulets();
            Amulet amulet = amulets.Find(a => a.Name == name);

            if (amulet == null)
            {
                Console.WriteLine("There's no amulet with given name" +
                                  "\nPress any key to continue");
                Console.ReadLine();
                return;
            }

            _amuletRepo.RemoveAmulet(amulet);
        }

        public bool CheckIfElementsInDatabase()
        {
            List<Hero> heroes = _heroRepo.RetrieveHeroes();
            List<Monster> monsters = _monsterRepo.RetrieveMonsters();
            List<Weapon> weapons = _weaponRepo.RetrieveWeapons();
            List<Armor> armors = _armorRepo.RetrieveArmors();
            List<Amulet> amulets = _amuletRepo.RetrieveAmulets();

            bool missingElementsInDatabase = false;

            if (heroes.Count == 0)
            {
                Console.WriteLine("No Heroes in database");
                missingElementsInDatabase = true;
            }

            if (monsters.Count == 0)
            {
                Console.WriteLine("No Monsters in database");
                missingElementsInDatabase = true;
            }

            if (weapons.Count == 0)
            {
                Console.WriteLine("No Weapons in database");
                missingElementsInDatabase = true;
            }

            if (armors.Count == 0)
            {
                Console.WriteLine("No Armors in database");
                missingElementsInDatabase = true;
            }

            if (amulets.Count == 0)
            {
                Console.WriteLine("No Amulets in database");
                missingElementsInDatabase = true;
            }

            return missingElementsInDatabase;
        }
    }
}

