﻿using System;
using DataAccess;
using DataAccess.Entities;
using DataAccess.Repos;

namespace BusinessLogic
{
    public class LevelManager : ILevelManager
    {
        private readonly IHeroRepo _heroRepo;
        private int _currentAmountToLevelUp;

        public LevelManager(IHeroRepo heroRepo)
        {
            _heroRepo = heroRepo;
            _currentAmountToLevelUp = GameRules.AmountOfExpToLevelUp;
        }

        public void CheckIfHeroLevelUp(Hero hero)
        {
            int lvlUpPoints = 0;

            while (hero.Exp >= _currentAmountToLevelUp && hero.Level != GameRules.MaxLevel)
            {
                _currentAmountToLevelUp = _currentAmountToLevelUp * GameRules.AmountOfExpToLevelUpMulti;
                hero.Level++;
                lvlUpPoints += GameRules.AmountOfPointsAfterLvlUp;
            }

            if (lvlUpPoints != 0)
            {
                PlayerLog.AddToLogAndConsole($"You level up to {hero.Level}");
                AttributeRaise(hero, lvlUpPoints);
            }
        }

        public void AttributeRaise(Hero hero, int lvlUpPoints)
        {
            int strengthToRaise = 0;
            int magicToRaise = 0;
            int dexterityToRaise = 0;
            int vitalityToRaise = 0;

            bool doContinue = true;

            Console.WriteLine($"\nRaise your attributes" +
                              $"\nPoints to spend : {lvlUpPoints}");

            while (doContinue)
            {
                int lvlUpPointsToSpend = lvlUpPoints;
                Console.Write("\nHow many points spend on Strength :");
                strengthToRaise = ExceptionPrevention.CheckVariable(Console.ReadLine(), 0, lvlUpPointsToSpend);
                strengthToRaise = AttributeMax(strengthToRaise, hero.Strength, GameRules.MaxStrength);
                lvlUpPointsToSpend -= strengthToRaise;

                Console.WriteLine($"Points left : {lvlUpPointsToSpend}");

                Console.Write("\nHow many points spend on Magic :");
                magicToRaise = ExceptionPrevention.CheckVariable(Console.ReadLine(), 0, lvlUpPointsToSpend);
                magicToRaise = AttributeMax(magicToRaise, hero.Magic, GameRules.MaxMagic);
                lvlUpPointsToSpend -= magicToRaise;

                Console.WriteLine($"Points left : {lvlUpPointsToSpend}");

                Console.Write("\nHow many points spend on Dexterity :");
                dexterityToRaise = ExceptionPrevention.CheckVariable(Console.ReadLine(), 0, lvlUpPointsToSpend);
                dexterityToRaise = AttributeMax(dexterityToRaise, hero.Dexterity, GameRules.MaxDexterity);
                lvlUpPointsToSpend -= dexterityToRaise;

                Console.WriteLine($"Points left : {lvlUpPointsToSpend}");

                Console.Write("\nHow many points spend on Vitality :");
                vitalityToRaise = ExceptionPrevention.CheckVariable(Console.ReadLine(), lvlUpPointsToSpend, lvlUpPointsToSpend);
                vitalityToRaise = AttributeMax(vitalityToRaise, hero.Vitality, GameRules.MaxVitality);

                Console.WriteLine("Are you sure? (y/n)");
                string yesOrNo = ExceptionPrevention.CheckYesOrNo(Console.ReadLine());

                if (yesOrNo == "y")
                {
                    doContinue = false;
                }
            }

            hero.Strength += strengthToRaise;
            hero.Magic += magicToRaise;
            hero.Dexterity += dexterityToRaise;
            hero.Vitality += vitalityToRaise;
            hero.FullHealth += GameRules.AttributesMultiplying(vitalityToRaise, GameRules.HeroVitMulti);
            hero.CurrentHealth += GameRules.AttributesMultiplying(vitalityToRaise, GameRules.HeroVitMulti);

            _heroRepo.UpdateHero(hero);

            PlayerLog.AddToLogAndConsole($"\nAttributes now:" +
                                         $"\nStrength : {hero.Strength}" +
                                         $"\nMagic : {hero.Magic}" +
                                         $"\nDexterity : {hero.Dexterity}" +
                                         $"\nVitality : {hero.Vitality}");
            Console.ReadLine();
        }

        private int AttributeMax(int attributeToRaise, int attributeCurrent, int maxAttributeValue)
        {
            if (maxAttributeValue < (attributeCurrent + attributeToRaise))
            {
                Console.WriteLine("You try to raise attribute above maximum value");
                int attributeToMax = maxAttributeValue - attributeCurrent;
                return attributeToMax;
            }

            return attributeToRaise;
        }
    }
}
