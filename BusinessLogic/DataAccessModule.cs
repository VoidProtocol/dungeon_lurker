﻿using DataAccess.Repos;
using Ninject.Modules;

namespace BusinessLogic
{
    public class DataAccessModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAmuletRepo>().To<AmuletRepo>();
            Bind<IArmorRepo>().To<ArmorRepo>();
            Bind<IHeroRepo>().To<HeroRepo>();
            Bind<IHighScoreRepo>().To<HighScoreRepo>();
            Bind<IMonsterRepo>().To<MonsterRepo>();
            Bind<IWeaponRepo>().To<WeaponRepo>();
        }
    }
}
