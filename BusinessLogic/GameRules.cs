﻿namespace BusinessLogic
{
    public class GameRules
    {
        public static int MaxStrength { get; set; } = 30;
        public static int MaxMagic { get; set; } = 30;
        public static int MaxDexterity { get; set; } = 30;
        public static int MaxVitality { get; set; } = 30;
        public static int AmountOfExpToLevelUp { get; set; } = 100;
        public static int AmountOfExpToLevelUpMulti { get; set; } =2;
        public static int MaxLevel { get; set; } = 20;
        public static int AmountOfPointsAfterLvlUp { get; set; } = 5;
        public static int HeroVitMulti { get; set; } = 10;
        public static int MonsterVitMulti { get; set; } = 10;
        public static int HeroDexMulti { get; set; } = 3;
        public static int MonsterDexMulti { get; set; } = 3;
        public static int HeroStrMulti { get; set; } = 3;
        public static int HeroMagMulti { get; set; } = 3;
        public static int MonsterStrMulti { get; set; } = 3;
        public static int MinDodgeChance { get; set; } = 1;
        public static int MaxDodgeChance { get; set; } = 80;
        public static int LowImpactMax { get; set; } = 50;
        public static int MidImpactMax { get; set; } = 85;
        public static int HighImpactMax { get; set; } = 100;

        public static int AttributesMultiplying(int attribute, double multiply)
        {
            double result = (double)attribute * multiply;
            return (int)result;
        }

        public static int PreventNumberOverLimit(int number, int minLimit, int maxLimit)
        {
            if (number < minLimit)
            {
                return minLimit;
            }
            if (number > maxLimit)
            {
                return maxLimit;
            }

            return number;
        }
    }
}
