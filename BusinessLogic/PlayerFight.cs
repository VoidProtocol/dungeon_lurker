﻿using System;
using DataAccess.Entities;

namespace BusinessLogic
{
    public class PlayerFight : IPlayerFight
    {
        private Random _random;
        private bool _battleContinue;

        public PlayerFight()
        {
            _random = new Random();
        }

        public void Battle(Hero hero, Monster monster)
        {
            PlayerLog.AddToLogAndConsole($"\nYou're attacked by {monster.Name} level: {monster.Level}!");

            _battleContinue = true;

            while (_battleContinue)
            {
                BattleActions action = ChooseAction();

                switch (action)
                {
                    case BattleActions.Attack:
                        AttackAction(hero, monster, true);
                        break;

                    case BattleActions.Spell:
                        AttackAction(hero, monster, false);
                        break;

                    case BattleActions.Run:
                        RunAction(hero, monster);
                        break;
                }

                hero.CurrentHealth = GameRules.PreventNumberOverLimit(hero.CurrentHealth, 0, Int32.MaxValue);
                monster.CurrentHealth = GameRules.PreventNumberOverLimit(monster.CurrentHealth, 0, Int32.MaxValue);

                Console.WriteLine($"\nHero HP: {hero.CurrentHealth}/{hero.FullHealth}" +
                                  $"\nMonster HP: {monster.CurrentHealth}/{monster.FullHealth}");

                if (hero.CurrentHealth <= 0 && monster.CurrentHealth <= 0)
                {
                    HeroAndMonsterKillEachOther(hero, monster);
                }
                else if (hero.CurrentHealth <= 0 && monster.CurrentHealth > 0)
                {
                    MonsterKillsHero(hero, monster);
                }
                else if (hero.CurrentHealth > 0 && monster.CurrentHealth <= 0)
                {
                    HeroKillsMonster(hero, monster);
                }
            }
        }

        private BattleActions ChooseAction()
        {
            Console.WriteLine("Choose your action:" +
                              "\n1.Attack" +
                              "\n2.Spell" +
                              "\n3.Run");
            int choice = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 3);
            return (BattleActions) choice;
        }

        private void AttackAction(Hero hero, Monster monster, bool isStrengthAttack)
        {
            if (DodgeCheck(hero.Dexterity, monster.Dexterity, GameRules.HeroDexMulti))
            {
                PlayerLog.AddToLogAndConsole($"You dodged {monster.Name}'s attack!");
            }
            else
            {
                MonsterDamageDealing(hero, monster);
            }

            if (DodgeCheck(hero.Dexterity, monster.Dexterity, GameRules.MonsterDexMulti))
            {
                PlayerLog.AddToLogAndConsole($"You missed {monster.Name}!");
            }
            else if (isStrengthAttack)
            {
                HeroStrengthDamageDealing(hero, monster);
            }
            else
            {
                HeroMagicDamageDealing(hero, monster);
            }
        }

        private void RunAction(Hero hero, Monster monster)
        {
            if (DodgeCheck(hero.Dexterity, monster.Dexterity, 5))
            {
                PlayerLog.AddToLogAndConsole("You escaped!");
                _battleContinue = false;
            }
            else
            {
                PlayerLog.AddToLogAndConsole("Escape attempt failed!");
                MonsterDamageDealing(hero, monster);
            }
        }

        private void HeroStrengthDamageDealing(Hero hero, Monster monster)
        {
            int heroStrengthDmg =
                (GameRules.AttributesMultiplying(hero.Strength, GameRules.HeroStrMulti) - monster.StrengthDefense);

            if (heroStrengthDmg <= 0)
            {
                heroStrengthDmg = 1;
            }

            monster.CurrentHealth -= heroStrengthDmg;
            PlayerLog.AddToLogAndConsole($"You deal {monster.Name} {heroStrengthDmg}dmg!");
        }

        private void HeroMagicDamageDealing(Hero hero, Monster monster)
        {
            Spell spell = (Spell)_random.Next(1, 6);
            double magicRandomMultiply = _random.NextDouble() + 0.5;

            int heroBaseMagicDmg = (GameRules.AttributesMultiplying(hero.Magic, GameRules.HeroMagMulti));
            int heroMagicDmg = 
                (GameRules.AttributesMultiplying(heroBaseMagicDmg, magicRandomMultiply) - monster.MagicDefense);

            if (heroMagicDmg <= 0)
            {
                heroMagicDmg = 1;
            }

            monster.CurrentHealth -= heroMagicDmg;
            PlayerLog.AddToLogAndConsole($"You cast {spell.ToString()} and deal {monster.Name} {heroMagicDmg}dmg!");
        }

        private void MonsterDamageDealing(Hero hero, Monster monster)
        {
            int monsterDmg =
                (GameRules.AttributesMultiplying(monster.Strength, GameRules.MonsterStrMulti) - hero.StrengthDefense);

            if (monsterDmg <= 0)
            {
                monsterDmg = 1;
            }

            hero.CurrentHealth -= monsterDmg;
            PlayerLog.AddToLogAndConsole($"{monster.Name} deals you {monsterDmg}dmg!");
        }

        private bool DodgeCheck(int defenderDex, int attackerDex, int dexMulti)
        {
            int dodgeChance = (defenderDex - attackerDex);
            dodgeChance = GameRules.AttributesMultiplying(dodgeChance, dexMulti);

            switch (dodgeChance)
            {
                case int n when (n < GameRules.MinDodgeChance):
                    dodgeChance = GameRules.MinDodgeChance;
                    break;
                case int n when (n > GameRules.MaxDodgeChance):
                    dodgeChance = GameRules.MaxDodgeChance;
                    break;
            }

            if (dodgeChance >= _random.Next(1,101))
            {
                return true;
            }

                return false;
        }

        private void HeroAndMonsterKillEachOther(Hero hero, Monster monster)
        {
            monster.CurrentHealth = monster.FullHealth;

            Adventure.CurrentHeroAlive = false;
            _battleContinue = false;

            PlayerLog.AddToLogAndConsole($"You and {monster.Name} deal finishing blows to " +
                                         $"each other at the same time!");
        }

        private void MonsterKillsHero(Hero hero, Monster monster)
        {
            monster.CurrentHealth = monster.FullHealth;

            Adventure.CurrentHeroAlive = false;
            _battleContinue = false;

            PlayerLog.AddToLogAndConsole($"You are killed by {monster.Name}!");
        }

        private void HeroKillsMonster(Hero hero, Monster monster)
        {
            monster.CurrentHealth = monster.FullHealth;
            hero.Exp += monster.ExpWorth;
            hero.Points += monster.PointsWorth;

            _battleContinue = false;

            PlayerLog.AddToLogAndConsole($"You have defeated {monster.Name}!");
        }
    }
}
