﻿using NUnit.Framework;

namespace BusinessLogic.Tests
{
    class TestPlayerFight
    {
        [TestFixture]
        class TestAdventure
        {
            private BattleActions _battleActions;

            [SetUp]
            public void SetUp()
            {
                _battleActions = new BattleActions();
            }

            [Test]
            public void PlayerGetActionHeChoose()
            {
                var battleActions = BattleActions.Attack;
                var playerChoice = 1;

                Assert.That(playerChoice, Is.EqualTo(battleActions));
            }
        }
    }
}
