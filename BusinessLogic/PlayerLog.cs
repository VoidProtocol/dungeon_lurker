﻿using System;
using System.Text;

namespace BusinessLogic
{
    public class PlayerLog
    {
        public static StringBuilder Log = new  StringBuilder();

        public static void AddToLogAndConsole(string newLog)
        {
            Log.Append($"\n{DateTime.Now.ToString("G")}: {newLog}");
            Console.WriteLine(newLog);
        }
    }
}
