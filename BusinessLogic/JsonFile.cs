﻿using System.IO;
using Newtonsoft.Json;

namespace BusinessLogic
{
    public class JsonFile
    {
        public static void SerializeJson(string log, string filePath)
        {
            JsonSerializer jsonSerializer = new JsonSerializer();
            using (StreamWriter streamWriter = new StreamWriter(filePath))
            {
                jsonSerializer.Serialize(streamWriter, log);
            }
        }
    }
}
