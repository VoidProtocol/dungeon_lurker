﻿namespace BusinessLogic
{
    public enum Treasure
    {
        None,
        Gold,
        Diamonds,
        Silver,
        Chocolate,
        Coins
    }
}