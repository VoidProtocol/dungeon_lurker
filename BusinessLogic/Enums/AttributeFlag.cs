﻿namespace BusinessLogic
{
    public enum AttributeFlag
    {
        None,
        Strength,
        Magic,
        Dexterity,
        Vitality
    }
}