﻿namespace BusinessLogic
{
    public enum BattleActions
    {
        None,
        Attack,
        Spell,
        Run,
    }
}
