﻿namespace BusinessLogic
{
    public enum FateImpact
    {
        None,
        Low,
        Medium,
        High
    }
}