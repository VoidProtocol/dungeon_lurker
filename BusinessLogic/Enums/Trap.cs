﻿namespace BusinessLogic
{
    public enum Trap
    {
        None,
        StrengthLost,
        MagicLost,
        DexterityLost,
        VitalityLost,
        WeaponLost,
        ArmorLost,
        AmuletLost
    }
}