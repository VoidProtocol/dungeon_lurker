﻿namespace BusinessLogic
{
    public enum Pest
    {
        None,
        Snake,
        Rat,
        Duck,
        Cat,
        Rooster
    }
}