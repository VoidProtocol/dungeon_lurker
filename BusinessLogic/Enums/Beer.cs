﻿namespace BusinessLogic
{
    public enum Beer
    {
        None,
        Guinness,
        Okocim,
        Leszek,
        Specjal,
        Tatra,
        Kozel
    }
}