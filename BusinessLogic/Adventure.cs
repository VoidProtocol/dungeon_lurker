﻿using System;
using System.Collections.Generic;
using System.IO;
using DataAccess.Entities;
using DataAccess.Repos;

namespace BusinessLogic
{
    public class Adventure : IAdventure
    {
        private readonly IHeroRepo _heroRepo;
        private readonly IMonsterRepo _monsterRepo;
        private readonly IWeaponRepo _weaponRepo;
        private readonly IArmorRepo _armorRepo;
        private readonly IAmuletRepo _amuletRepo;
        private readonly IPlayerFight _playerFight;
        private readonly ILevelManager _levelManager;
        private readonly IGameCreation _gameCreation;

        private Hero _hero;
        private Monster _monster;
        private Weapon _weapon;
        private Armor _armor;
        private Amulet _amulet;
        private Random _random;

        public static bool CurrentHeroAlive { get; set; }

        public Adventure(IHeroRepo heroRepo, IMonsterRepo monsterRepo, IWeaponRepo weaponRepo, IArmorRepo armorRepo, 
            IAmuletRepo amuletRepo, IPlayerFight playerFight, ILevelManager levelManager, IGameCreation gameCreation)
        {
            _heroRepo = heroRepo;
            _monsterRepo = monsterRepo;
            _weaponRepo = weaponRepo;
            _armorRepo = armorRepo;
            _amuletRepo = amuletRepo;
            _playerFight = playerFight;
            _levelManager = levelManager;
            _gameCreation = gameCreation;
            _random = new Random();
        }

        public void ChooseHero(string name)
        {
            List<Hero> Heroes = _heroRepo.RetrieveHeroes();
            _hero = Heroes.Find(hero => hero.Name == name);

            if (_hero == null)
            {
                Console.WriteLine("There's no hero with given name" +
                                  "\nPress any key to continue");
                Console.ReadLine();
                return;
            }

            while (!_hero.Alive)
            {
                Console.WriteLine("\nChosen hero is dead" +
                                  "\n1.Choose another one" +
                                  "\n2.Revive this one (points will be erased)");

                int choice = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 2);

                if (choice == 1)
                {
                    Console.WriteLine("Choose your hero (by name)");
                    string anotherName = Console.ReadLine();
                    _hero = Heroes.Find(hero => hero.Name == anotherName);
                }
                else
                {
                    _hero.Alive = true;
                    _hero.CurrentHealth = _hero.FullHealth;
                    _hero.Points = 0;
                    _heroRepo.UpdateHero(_hero);
                }
            }

            CurrentHeroAlive = true;
            FateDraw();
        }

        private void FateDraw()
        {
            bool continueGame = true;

            PlayerLog.AddToLogAndConsole("You enter the dungeon and start your journey, good luck!");

            while (continueGame)
            {
                _levelManager.CheckIfHeroLevelUp(_hero);

                switch (_random.Next(1, 101))
                {

                    case int n when (n > 0 && n <= 40):
                        HeroWearEquipmentBeforeFight();
                        EntityDraw(FateImpactDraw(), _monsterRepo);
                        _playerFight.Battle(_hero, _monster);
                        HeroRemoveEquipmentAfterFight();
                        ItemDropFromMonster();
                        break;

                    case int n when (n > 40 && n <= 50):
                        EventPest();
                        break;

                    case int n when (n > 50 && n <= 55):
                        EventTrap();
                        break;

                    case int n when (n > 55 && n <= 60):
                        EventTreasure();
                        break;

                    case int n when (n > 60 && n <= 76):
                        EventBeer();
                        break;

                    case int n when (n > 76 && n <= 84):
                        EventWeapon();
                        break;

                    case int n when (n > 84 && n <= 92):
                        EventArmor();
                        break;

                    case int n when (n > 92 && n <= 100):
                        EventAmulet();
                        break;
                }

                GameOverCheck();
                continueGame = GameContinueCheck();

                Console.Write("\nPress any key to continue");
                Console.ReadLine();
            }
        }

        private FateImpact FateImpactDraw()
        {
            switch (_random.Next(1, 101))
            {
                case int n when (n > 0 && n <= GameRules.LowImpactMax):
                    return FateImpact.Low;
                case int n when (n > GameRules.LowImpactMax && n <= GameRules.MidImpactMax):
                    return FateImpact.Medium;
                case int n when (n > GameRules.MidImpactMax && n <= GameRules.HighImpactMax):
                    return FateImpact.High;
                default:
                    return FateImpact.None;
            }
        }

        private void EntityDraw(FateImpact fateImpact, IMonsterRepo monsterRepo)
        {
            List<Monster> monsters = monsterRepo.RetrieveMonsters();

            switch (fateImpact)
            {
                case FateImpact.Low:
                    _monster = monsters.Find(monster => monster.Level > 0 && monster.Level <= 10);
                    if (_monster == null)
                    {
                        goto case FateImpact.High;
                    }
                    break;

                case FateImpact.Medium:
                    _monster = monsters.Find(monster => monster.Level > 10 && monster.Level <= 17);
                    if (_monster == null)
                    {
                        goto case FateImpact.Low;
                    }
                    break;

                case FateImpact.High:
                    _monster = monsters.Find(monster => monster.Level > 17 && monster.Level <= 20);
                    if (_monster == null)
                    {
                        goto case FateImpact.Medium;
                    }
                    break;
            }
        }

        private void EntityDraw(FateImpact fateImpact, IWeaponRepo weaponRepo)
        {
            List<Weapon> weapons = weaponRepo.RetrieveWeapons();

            switch (fateImpact)
            {
                case FateImpact.Low:
                    _weapon = weapons.Find(weapon => weapon.Level > 0 && weapon.Level <= 10);
                    if (_weapon == null)
                    {
                        goto case FateImpact.High;
                    }
                    break;

                case FateImpact.Medium:
                    _weapon = weapons.Find(weapon => weapon.Level > 10 && weapon.Level <= 17);
                    if (_weapon == null)
                    {
                        goto case FateImpact.Low;
                    }
                    break;

                case FateImpact.High:
                    _weapon = weapons.Find(weapon => weapon.Level > 17 && weapon.Level <= 20);
                    if (_weapon == null)
                    {
                        goto case FateImpact.Medium;
                    }
                    break;
            }
        }

        private void EntityDraw(FateImpact fateImpact, IArmorRepo armorRepo)
        {
            List<Armor> armors = armorRepo.RetrieveArmors();

            switch (fateImpact)
            {
                case FateImpact.Low:
                    _armor = armors.Find(armor => armor.Level > 0 && armor.Level <= 10);
                    if (_armor == null)
                    {
                        goto case FateImpact.High;
                    }
                    break;

                case FateImpact.Medium:
                    _armor = armors.Find(armor => armor.Level > 10 && armor.Level <= 17);
                    if (_armor == null)
                    {
                        goto case FateImpact.Low;
                    }
                    break;

                case FateImpact.High:
                    _armor = armors.Find(armor => armor.Level > 17 && armor.Level <= 20);
                    if (_armor == null)
                    {
                        goto case FateImpact.Medium;
                    }
                    break;
            }
        }

        private void EntityDraw(FateImpact fateImpact, IAmuletRepo amuletRepo)
        {
            List<Amulet> amulets = amuletRepo.RetrieveAmulets();

            switch (fateImpact)
            {
                case FateImpact.Low:
                    _amulet = amulets.Find(amulet => amulet.Level > 0 && amulet.Level <= 10);
                    if (_amulet == null)
                    {
                        goto case FateImpact.High;
                    }
                    break;

                case FateImpact.Medium:
                    _amulet = amulets.Find(amulet => amulet.Level > 10 && amulet.Level <= 17);
                    if (_amulet == null)
                    {
                        goto case FateImpact.Low;
                    }
                    break;

                case FateImpact.High:
                    _amulet = amulets.Find(amulet => amulet.Level > 17 && amulet.Level <= 20);
                    if (_amulet == null)
                    {
                        goto case FateImpact.Medium;
                    }
                    break;
            }
        }

        private void EventBeer()
        {
            Beer beer = (Beer) _random.Next(1, 7);
            int healthModifier = _random.Next(10, 31);
            _hero.CurrentHealth += healthModifier;

            if (_hero.CurrentHealth > _hero.FullHealth)
            {
                _hero.CurrentHealth = _hero.FullHealth;
            }

            PlayerLog.AddToLogAndConsole($"\nYou drink delicious {beer.ToString()}!" +
                                         $"\nYou replenish {healthModifier} HP" +
                                         $"\nHP: {_hero.CurrentHealth}/{_hero.FullHealth}");
        }

        private void EventWeapon()
        {
            EntityDraw(FateImpactDraw(), _weaponRepo);

            if (_hero.CurrentWeapon == _weapon)
            {
                return;
            }

            PlayerLog.AddToLogAndConsole($"\nYou find {_weapon.Name}!" +
                                         $"\nIt gives additional {_weapon.StrengthModifier} strength if you wear it");

            if (_hero.CurrentWeapon == null)
            {
                _hero.CurrentWeapon = _weapon;
                PlayerLog.AddToLogAndConsole($"You equip {_weapon.Name}" +
                                             $"\nStrength: {_hero.CurrentWeapon.StrengthModifier}");

            }
            else
            {
                Console.WriteLine($"Your actual weapon gives you additional " +
                                  $"{_hero.CurrentWeapon.StrengthModifier} strength" +
                                  $"\nDo you want to change it to {_weapon.Name} that gives " +
                                  $"{_weapon.StrengthModifier}? (y/n)");
                string yesOrNo = ExceptionPrevention.CheckYesOrNo(Console.ReadLine());

                if (yesOrNo == "y")
                {
                    PlayerLog.AddToLogAndConsole($"You equip {_weapon.StrengthModifier}" +
                                                 $"\nStrength: {_hero.CurrentWeapon.StrengthModifier}");
                }
                else
                {
                    PlayerLog.AddToLogAndConsole($"You stay with your old weapon" +
                                                 $"\nStrength: {_hero.CurrentWeapon.StrengthModifier}");
                }
            }
        }

        private void EventArmor()
        {
            EntityDraw(FateImpactDraw(), _armorRepo);

            if (_hero.CurrentArmor == _armor)
            {
                return;
            }

            PlayerLog.AddToLogAndConsole($"\nYou find {_armor.Name}!" +
                                         $"\nIt gives {_armor.DefenseModifier} defense if you wear it");

            if (_hero.CurrentArmor == null)
            {
                _hero.CurrentArmor = _armor;
                PlayerLog.AddToLogAndConsole($"You equip {_armor.Name}" +
                                             $"\nDefense: {_hero.CurrentArmor.DefenseModifier}");

            }
            else
            {
                Console.WriteLine($"Your actual armor gives you {_hero.CurrentArmor.DefenseModifier} defense" +
                                  $"\nDo you want to change it to {_armor.Name} that gives " +
                                  $"{_armor.DefenseModifier}? (y/n)");
                string yesOrNo = ExceptionPrevention.CheckYesOrNo(Console.ReadLine());

                if (yesOrNo == "y")
                {
                    PlayerLog.AddToLogAndConsole($"You equip {_armor.DefenseModifier}" +
                                                 $"\nDefense: {_hero.CurrentArmor.DefenseModifier}");
                }
                else
                {
                    PlayerLog.AddToLogAndConsole($"You stay with your old armor" +
                                                 $"\nDefense: {_hero.CurrentArmor.DefenseModifier}");
                }
            }
        }

        private void EventAmulet()
        {
            EntityDraw(FateImpactDraw(), _amuletRepo);

            if (_hero.CurrentAmulet == _amulet)
            {
                return;
            }

            PlayerLog.AddToLogAndConsole($"\nYou find {_amulet.Name}!" +
                                         $"\nIt modifies {_amulet.AttributeType} by {_amulet.AttributeModifier} " +
                                         $"if you wear it");

            if (_hero.CurrentAmulet == null)
            {
                _hero.CurrentAmulet = _amulet;
                PlayerLog.AddToLogAndConsole($"You equip {_amulet.Name}" +
                                             $"\nAttribute: {_hero.CurrentAmulet.AttributeModifier} " +
                                             $"{_hero.CurrentAmulet.AttributeType}");
            }
            else
            {
                Console.WriteLine($"Your actual amulet modifies your {_hero.CurrentAmulet.AttributeType} by " +
                                  $"{_hero.CurrentAmulet.AttributeModifier} " +
                                  $"\nDo you want to change it to {_amulet.Name} that modifies " +
                                  $"{_amulet.AttributeType} by {_amulet.AttributeModifier}? (y/n)");
                string yesOrNo = ExceptionPrevention.CheckYesOrNo(Console.ReadLine());

                if (yesOrNo == "y")
                {
                    PlayerLog.AddToLogAndConsole($"You equip {_amulet.Name}" +
                                                 $"\nAttribute: {_hero.CurrentAmulet.AttributeModifier} " +
                                                 $"{_hero.CurrentAmulet.AttributeType}");
                }
                else
                {
                    PlayerLog.AddToLogAndConsole($"You stay with your old amulet" +
                                                 $"\nAttribute: {_hero.CurrentAmulet.AttributeModifier} " +
                                                 $"{_hero.CurrentAmulet.AttributeType}");
                }
            }
        }

        private void EventPest()
        {
            Pest pest = (Pest)_random.Next(1, 6);
            int pestBite = _random.Next(1, 21);
            _hero.CurrentHealth -= pestBite;

            if (_hero.CurrentHealth <= 0)
            {
                _hero.CurrentHealth = GameRules.PreventNumberOverLimit(_hero.CurrentHealth, 0, Int32.MaxValue);
                CurrentHeroAlive = false;
                PlayerLog.AddToLogAndConsole($"\nOh no! Evil {pest.ToString()} bites you dealing {pestBite} damage and now you're dead!" +
                                             $"\nWhat a pathetic way to die...");
            }
            else
            {
                PlayerLog.AddToLogAndConsole($"\nOh no! Evil {pest.ToString()} bites you dealing {pestBite} damage!" +
                                             $"\nHP: {_hero.CurrentHealth}/{_hero.FullHealth}");
            }
        }

        private void EventTrap()
        {
            Trap trap = (Trap)_random.Next(1, 8);

            switch (trap)
            {
                case Trap.StrengthLost:
                    _hero.Strength = AttributeLost("Vegan trap", 1, AttributeFlag.Strength, _hero.Strength);
                    break;
                case Trap.MagicLost:
                    _hero.Magic = AttributeLost("Anti-Magic trap", 1, AttributeFlag.Magic, _hero.Magic);
                    break;
                case Trap.DexterityLost:
                    _hero.Dexterity = AttributeLost("Ice trap", 1, AttributeFlag.Dexterity, _hero.Dexterity);
                    break;
                case Trap.VitalityLost:
                    _hero.Vitality = AttributeLost("Fire trap", 1, AttributeFlag.Vitality, _hero.Vitality);
                    break;
                case Trap.WeaponLost:
                    ItemLost(_hero.CurrentWeapon);
                    break;
                case Trap.ArmorLost:
                    ItemLost(_hero.CurrentArmor);
                    break;
                case Trap.AmuletLost:
                    ItemLost(_hero.CurrentAmulet);
                    break;
            }
        }

        private int AttributeLost(string trapName, int trapModifier, AttributeFlag attribute, int heroAtt)
        {
            if (heroAtt <= trapModifier)
            {
                PlayerLog.AddToLogAndConsole($"\nYou activate {trapName} but you have too low {attribute.ToString()} " +
                                             $"to lose any");
                return heroAtt;
            }
            else
            {
                PlayerLog.AddToLogAndConsole($"\nYou activate {trapName} and lost {trapModifier} {attribute.ToString()}");
                int result = heroAtt - trapModifier;
                return result;
            }
        }

        private void ItemLost(Weapon weapon)
        {
            Pest pest = (Pest)_random.Next(1, 6);

            if (weapon == null)
            {
                PlayerLog.AddToLogAndConsole("\nYou were about to lose weapon but you don't have any");
            }
            else
            {
                PlayerLog.AddToLogAndConsole($"\n{pest.ToString()} stole your weapon!");
                _hero.CurrentWeapon = null;
            }
        }

        private void ItemLost(Armor armor)
        {
            Pest pest = (Pest)_random.Next(1, 6);

            if (armor == null)
            {
                PlayerLog.AddToLogAndConsole("\nYou were about to lose armor but you don't have any");
            }
            else
            {
                PlayerLog.AddToLogAndConsole($"\n{pest.ToString()} stole your armor!");
                _hero.CurrentArmor = null;
            }
        }

        private void ItemLost(Amulet amulet)
        {
            Pest pest = (Pest)_random.Next(1, 6);

            if (amulet == null)
            {
                PlayerLog.AddToLogAndConsole(@"You were about to lose amulet but you don't have any ¯\_(ツ)_/¯");
            }
            else
            {
                PlayerLog.AddToLogAndConsole($"\n{pest.ToString()} stole your amulet!");
                _hero.CurrentAmulet = null;
            }
        }

        private void EventTreasure()
        {
            Treasure treasure = (Treasure)_random.Next(1, 6);
            int treasureValue = _random.Next(10, 100);
            _hero.Points += treasureValue;

            PlayerLog.AddToLogAndConsole($"\nYou find lots of {treasure.ToString()}!" +
                                         $"\nIt gives you {treasureValue} points" +
                                         $"\nPoints: {_hero.Points}");
        }

        private void HeroWearEquipmentBeforeFight()
        {
            if (_hero.CurrentWeapon != null)
            {
                _hero.Strength += _hero.CurrentWeapon.StrengthModifier;
            }

            if (_hero.CurrentArmor != null)
            {
                _hero.StrengthDefense += _hero.CurrentArmor.DefenseModifier;
            }

            if (_hero.CurrentAmulet != null)
            {
                string attributeType = _hero.CurrentAmulet.AttributeType;

                switch (attributeType)
                {
                    case "Strength":
                        _hero.Strength += GameRules.PreventNumberOverLimit(_hero.CurrentAmulet.AttributeModifier, 1, Int32.MaxValue);
                        break;

                    case "Magic":
                        _hero.Magic += GameRules.PreventNumberOverLimit(_hero.CurrentAmulet.AttributeModifier, 1, Int32.MaxValue);
                        break;

                    case "Dexterity":
                        _hero.Dexterity += GameRules.PreventNumberOverLimit(_hero.CurrentAmulet.AttributeModifier, 1, Int32.MaxValue);
                        break;

                    case "Vitality":
                        _hero.Vitality += GameRules.PreventNumberOverLimit(_hero.CurrentAmulet.AttributeModifier, 1, Int32.MaxValue);
                        _hero.FullHealth += GameRules.AttributesMultiplying(_hero.CurrentAmulet.AttributeModifier,
                            GameRules.HeroVitMulti);
                        _hero.CurrentHealth += GameRules.AttributesMultiplying(_hero.CurrentAmulet.AttributeModifier,
                            GameRules.HeroVitMulti);

                        if (_hero.CurrentHealth <= 0)
                        {
                            _hero.CurrentHealth = 1;
                        }
                        break;
                }
            }
        }

        private void HeroRemoveEquipmentAfterFight()
        {
            if (_hero.CurrentWeapon != null)
            {
                _hero.Strength -= _hero.CurrentWeapon.StrengthModifier;
            }

            if (_hero.CurrentArmor != null)
            {
                _hero.StrengthDefense -= _hero.CurrentArmor.DefenseModifier;
            }

            if (_hero.CurrentAmulet != null)
            {
                string attributeType = _hero.CurrentAmulet.AttributeType;

                switch (attributeType)
                {
                    case "Strength":
                        _hero.Strength -= GameRules.PreventNumberOverLimit(_hero.CurrentAmulet.AttributeModifier, 1, Int32.MaxValue);
                        break;

                    case "Magic":
                        _hero.Magic -= GameRules.PreventNumberOverLimit(_hero.CurrentAmulet.AttributeModifier, 1, Int32.MaxValue);
                        break;

                    case "Dexterity":
                        _hero.Dexterity -= GameRules.PreventNumberOverLimit(_hero.CurrentAmulet.AttributeModifier, 1, Int32.MaxValue);
                        break;

                    case "Vitality":
                        _hero.Vitality -= GameRules.PreventNumberOverLimit(_hero.CurrentAmulet.AttributeModifier, 1, Int32.MaxValue);
                        _hero.FullHealth -= GameRules.AttributesMultiplying(_hero.CurrentAmulet.AttributeModifier,
                            GameRules.HeroVitMulti);
                        _hero.CurrentHealth -= GameRules.AttributesMultiplying(_hero.CurrentAmulet.AttributeModifier,
                            GameRules.HeroVitMulti);

                        if (_hero.CurrentHealth <= 0)
                        {
                            _hero.CurrentHealth = 1;
                        }
                        break;
                }
            }
        }

        private void ItemDropFromMonster()
        {
            if (CurrentHeroAlive)
            {
                switch (_random.Next(1, 101))
                {
                    case int n when (n > 0 && n <= 70):
                        break;

                    case int n when (n > 70 && n <= 80):
                        PlayerLog.AddToLogAndConsole("Something drops from monster");
                        EventWeapon();
                        break;

                    case int n when (n > 80 && n <= 90):
                        PlayerLog.AddToLogAndConsole("Something heavy drops from monster");
                        EventArmor();
                        break;

                    case int n when (n > 90 && n <= 100):
                        PlayerLog.AddToLogAndConsole("Something shiny drops from monster");
                        EventAmulet();
                        break;
                }
            }
        }

        private bool GameContinueCheck()
        {
            if (!CurrentHeroAlive)
            {
                return false;
            }

            return true;
        }

        private void GameOverCheck()
        {
            if (!CurrentHeroAlive)
            {
                _hero.Alive = false;
                _heroRepo.UpdateHero(_hero);

                PlayerLog.AddToLogAndConsole($"\nYour Journey ends here {_hero.Name}, Farewell! " +
                                  $"Points: {_hero.Points}");

                string filePathHighScore = Directory.GetCurrentDirectory() + "\\HighScore\\" + _hero.Name + ".json";

                FileInfo file = new FileInfo(filePathHighScore);
                file.Directory.Create();
                JsonFile.SerializeJson(PlayerLog.Log.ToString(), filePathHighScore);

                _gameCreation.CreateNewHighScore(_hero.Name, _hero.Points, filePathHighScore);
                Console.ReadLine();
            }
        }

    }
}
