﻿using System;

namespace BusinessLogic
{
    public class ExceptionPrevention
    {
        public static int CheckVariable(string value, int minValue, int maxValue)
        {
            bool isValid = int.TryParse(value, out int result);
            while (!isValid || result < minValue || result > maxValue)
            {
                Console.WriteLine("Incorrect value, please try again");
                value = Console.ReadLine();
                isValid = int.TryParse(value, out result);
            }

            return result;
        }

        public static string CheckYesOrNo(string value)
        {
            value.ToLower();

            while (value != "y" && value != "n")
            {
                Console.WriteLine($"Please type {"y"}(yes) or {"n"}(no)");
                value = Console.ReadLine();
                value.ToLower();
            }

            return value;
        }

        public static string StringLimit(string name)
        {
            while (name.Length > 20 || name.StartsWith(" ") || name.EndsWith(" ") || String.IsNullOrEmpty(name))
            {
                Console.WriteLine("Your name is incorrect, please provide new one with maximum 10 letters");
                name = Console.ReadLine();
            }

            return name;
        }
    }
}
