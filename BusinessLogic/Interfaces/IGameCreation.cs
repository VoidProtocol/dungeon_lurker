﻿namespace BusinessLogic
{
    public interface IGameCreation
    {
        bool CheckIfElementsInDatabase();
        void CreateNewAmulet(string name, string attributeType, int attributeModifier, int level);
        void CreateNewArmor(string name, int defenseModifier, int level);
        void CreateNewHero(string name);
        void CreateNewHighScore(string name, int points, string filePath);
        void CreateNewMonster(string name, int strength, int dexterity, int vitality, int strengthDefense, int expWorth, int level, int pointsWorth);
        void CreateNewWeapon(string name, int strengthModifier, int level);
        void DeleteAmulet(string name);
        void DeleteArmor(string name);
        void DeleteHero(string name);
        void DeleteMonster(string name);
        void DeleteWeapon(string name);
        void DisplayAmulets();
        void DisplayArmors();
        void DisplayHeroes();
        void DisplayHighScores();
        void DisplayMonsters();
        void DisplayWeapons();
    }
}