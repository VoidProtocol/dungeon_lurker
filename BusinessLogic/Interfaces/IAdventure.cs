﻿namespace BusinessLogic
{
    public interface IAdventure
    {
        void ChooseHero(string name);
    }
}