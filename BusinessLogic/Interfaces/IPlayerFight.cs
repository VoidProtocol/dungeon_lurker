﻿using DataAccess.Entities;

namespace BusinessLogic
{
    public interface IPlayerFight
    {
        void Battle(Hero hero, Monster monster);
    }
}