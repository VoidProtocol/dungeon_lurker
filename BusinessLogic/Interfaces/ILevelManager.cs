﻿using DataAccess.Entities;

namespace BusinessLogic
{
    public interface ILevelManager
    {
        void AttributeRaise(Hero hero, int lvlUpPoints);
        void CheckIfHeroLevelUp(Hero hero);
    }
}