﻿using System.Collections.Generic;
using System.Linq;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public class ArmorRepo : IArmorRepo
    {
        private DungeonLurkerDbContext _dbContext;

        public ArmorRepo(DungeonLurkerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void CreateArmor(Armor armor)
        {
            _dbContext.DbArmors.Add(armor);
            _dbContext.SaveChanges();
        }

        public List<Armor> RetrieveArmors()
        {
            return _dbContext.DbArmors.ToList();
        }

        public void UpdateArmor(Armor armor)
        {
            Armor dbArmor = _dbContext.DbArmors.Find(armor.Id);
            dbArmor = armor;
            _dbContext.SaveChanges();
        }

        public void RemoveArmor(Armor armor)
        {
            _dbContext.DbArmors.Remove(armor);
            _dbContext.SaveChanges();
        }
    }
}
