﻿using System.Collections.Generic;
using System.Linq;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public class AmuletRepo : IAmuletRepo
    {
        private DungeonLurkerDbContext _dbContext;

        public AmuletRepo(DungeonLurkerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void CreateAmulet(Amulet amulet)
        {
            _dbContext.DbAmulets.Add(amulet);
            _dbContext.SaveChanges();
        }

        public List<Amulet> RetrieveAmulets()
        {
            return _dbContext.DbAmulets.ToList();
        }

        public void UpdateAmulet(Amulet amulet)
        {
            Amulet dbAmulet = _dbContext.DbAmulets.Find(amulet.Id);
            dbAmulet = amulet;
            _dbContext.SaveChanges();
        }

        public void RemoveAmulet(Amulet amulet)
        {
            _dbContext.DbAmulets.Remove(amulet);
            _dbContext.SaveChanges();
        }
    }
}
