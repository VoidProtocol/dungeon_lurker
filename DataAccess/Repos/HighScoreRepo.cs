﻿using System.Collections.Generic;
using System.Linq;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public class HighScoreRepo : IHighScoreRepo
    {
        private DungeonLurkerDbContext _dbContext;

        public HighScoreRepo(DungeonLurkerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void CreateHighScore(HighScore highScore)
        {
            _dbContext.DbHighScores.Add(highScore);
            _dbContext.SaveChanges();
        }

        public List<HighScore> RetrieveHighScores()
        {
            return _dbContext.DbHighScores.ToList();
        }

        public void UpdateHighScore(HighScore highScore)
        {
            HighScore dbHighScore = _dbContext.DbHighScores.Find(highScore.Id);
            dbHighScore = highScore;
            _dbContext.SaveChanges();
        }

        public void RemoveHighScore(HighScore highScore)
        {
            _dbContext.DbHighScores.Remove(highScore);
            _dbContext.SaveChanges();
        }
    }
}
