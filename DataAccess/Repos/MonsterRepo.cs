﻿using System.Collections.Generic;
using System.Linq;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public class MonsterRepo : IMonsterRepo
    {
        private DungeonLurkerDbContext _dbContext;

        public MonsterRepo(DungeonLurkerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void CreateMonster(Monster monster)
        {
            _dbContext.DbMonsters.Add(monster);
            _dbContext.SaveChanges();
        }

        public List<Monster> RetrieveMonsters()
        {
            return _dbContext.DbMonsters.ToList();
        }

        public void UpdateMonster(Monster monster)
        {
            Monster dbMonster = _dbContext.DbMonsters.Find(monster.Id);
            dbMonster = monster;
            _dbContext.SaveChanges();
        }

        public void RemoveMonster(Monster monster)
        {
            _dbContext.DbMonsters.Remove(monster);
            _dbContext.SaveChanges();
        }
    }
}
