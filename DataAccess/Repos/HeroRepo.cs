﻿using System.Collections.Generic;
using System.Linq;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public class HeroRepo : IHeroRepo
    {
        private DungeonLurkerDbContext _dbContext;

        public HeroRepo(DungeonLurkerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void CreateHero(Hero hero)
        {
            _dbContext.DbHeroes.Add(hero);
            _dbContext.SaveChanges();
        }

        public List<Hero> RetrieveHeroes()
        {
            return _dbContext.DbHeroes.ToList();
        }

        public void UpdateHero(Hero hero)
        {
            Hero dbHero = _dbContext.DbHeroes.Find(hero.Id);
            dbHero = hero;
            _dbContext.SaveChanges();
        }

        public void RemoveHero(Hero hero)
        {
            _dbContext.DbHeroes.Remove(hero);
            _dbContext.SaveChanges();
        }
    }
}
