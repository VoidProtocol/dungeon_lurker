﻿using System.Collections.Generic;
using System.Linq;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public class WeaponRepo : IWeaponRepo
    {
        private DungeonLurkerDbContext _dbContext;

        public WeaponRepo(DungeonLurkerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void CreateWeapon(Weapon weapon)
        {
            _dbContext.DbWeapons.Add(weapon);
            _dbContext.SaveChanges();
        }

        public List<Weapon> RetrieveWeapons()
        {
            return _dbContext.DbWeapons.ToList();
        }

        public void UpdateWeapon(Weapon weapon)
        {
            Weapon dbWeapon = _dbContext.DbWeapons.Find(weapon.Id);
            dbWeapon = weapon;
            _dbContext.SaveChanges();
        }

        public void RemoveWeapon(Weapon weapon)
        {
            _dbContext.DbWeapons.Remove(weapon);
            _dbContext.SaveChanges();
        }
    }
}
