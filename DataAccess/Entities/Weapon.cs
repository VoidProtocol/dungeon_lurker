﻿namespace DataAccess.Entities
{
    public class Weapon
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int StrengthModifier { get; set; }
        public int Level { get; set; }
    }
}
