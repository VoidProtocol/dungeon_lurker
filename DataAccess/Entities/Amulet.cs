﻿namespace DataAccess.Entities
{
    public class Amulet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AttributeType { get; set; }
        public int AttributeModifier { get; set; }
        public int Level { get; set; }
    }
}
