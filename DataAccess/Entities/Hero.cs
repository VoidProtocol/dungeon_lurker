﻿namespace DataAccess.Entities
{
    public class Hero
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Strength { get; set; }
        public int Magic { get; set; }
        public int Dexterity { get; set; }
        public int Vitality { get; set; }
        public int FullHealth { get; set; }
        public int CurrentHealth { get; set; }
        public int StrengthDefense { get; set; }
        public int Exp { get; set; }
        public int Level { get; set; }
        public int Points { get; set; }
        public bool Alive { get; set; }
        public virtual Weapon CurrentWeapon { get; set; }
        public virtual Armor CurrentArmor { get; set; }
        public virtual Amulet CurrentAmulet { get; set; }
    }
}
