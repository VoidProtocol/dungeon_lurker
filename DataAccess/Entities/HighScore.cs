﻿namespace DataAccess.Entities
{
    public class HighScore
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Points { get; set; }
        public string Date { get; set; }
        public string FilePath { get; set; }
    }
}
