﻿namespace DataAccess.Entities
{
    public class Monster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Vitality { get; set; }
        public int FullHealth { get; set; }
        public int CurrentHealth { get; set; }
        public int StrengthDefense { get; set; }
        public int MagicDefense { get; set; }
        public int ExpWorth { get; set; }
        public int Level { get; set; }
        public int PointsWorth { get; set; }
    }
}
