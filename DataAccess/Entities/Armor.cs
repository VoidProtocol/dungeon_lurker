﻿namespace DataAccess.Entities
{
    public class Armor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public int DefenseModifier { get; set; }
    }
}
