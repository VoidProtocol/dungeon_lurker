﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public interface IArmorRepo
    {
        void CreateArmor(Armor armor);
        void RemoveArmor(Armor armor);
        List<Armor> RetrieveArmors();
        void UpdateArmor(Armor armor);
    }
}