﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public interface IMonsterRepo
    {
        void CreateMonster(Monster monster);
        void RemoveMonster(Monster monster);
        List<Monster> RetrieveMonsters();
        void UpdateMonster(Monster monster);
    }
}