﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public interface IHeroRepo
    {
        void CreateHero(Hero hero);
        void RemoveHero(Hero hero);
        List<Hero> RetrieveHeroes();
        void UpdateHero(Hero hero);
    }
}