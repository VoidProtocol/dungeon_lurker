﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public interface IAmuletRepo
    {
        void CreateAmulet(Amulet amulet);
        void RemoveAmulet(Amulet amulet);
        List<Amulet> RetrieveAmulets();
        void UpdateAmulet(Amulet amulet);
    }
}