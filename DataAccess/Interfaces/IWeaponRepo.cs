﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public interface IWeaponRepo
    {
        void CreateWeapon(Weapon weapon);
        void RemoveWeapon(Weapon weapon);
        List<Weapon> RetrieveWeapons();
        void UpdateWeapon(Weapon weapon);
    }
}