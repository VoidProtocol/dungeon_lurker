﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos
{
    public interface IHighScoreRepo
    {
        void CreateHighScore(HighScore highScore);
        void RemoveHighScore(HighScore highScore);
        List<HighScore> RetrieveHighScores();
        void UpdateHighScore(HighScore highScore);
    }
}