﻿using System.Data.Entity;
using DataAccess.Entities;

namespace DataAccess
{
    public class DungeonLurkerDbContext : DbContext
    {
        public DungeonLurkerDbContext() : base("DungeonLurkerDbConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DungeonLurkerDbContext, Migrations.Configuration>());
        }

        public DbSet<Hero> DbHeroes { get; set; }
        public DbSet<Monster> DbMonsters { get; set; }
        public DbSet<HighScore> DbHighScores { get; set; }
        public DbSet<Weapon> DbWeapons { get; set; }
        public DbSet<Armor> DbArmors { get; set; }
        public DbSet<Amulet> DbAmulets { get; set; }
    }
}
