using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace DataAccess.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DataAccess.DungeonLurkerDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "DataAccess.DungeonLurkerDbContext";
        }

        protected override void Seed(DataAccess.DungeonLurkerDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
